package za.co.radloff.configuration;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import za.co.radloff.config.StarterProperties;
import za.co.radloff.service.LogService;
import za.co.radloff.service.LogServiceImpl;

/**
 * Created by barry on 2017/06/14.
 */
@Configuration
@EnableConfigurationProperties(StarterProperties.class)
@ComponentScan("za.co.radloff.service")
public class StarterConfiguration {
    @Bean
    public LogService logService(){
        return new LogServiceImpl();
    }
}
