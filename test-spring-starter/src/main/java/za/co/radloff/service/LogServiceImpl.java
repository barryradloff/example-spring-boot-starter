package za.co.radloff.service;

import org.springframework.beans.factory.annotation.Autowired;
import za.co.radloff.config.StarterProperties;

/**
 * Created by barry on 2017/06/14.
 */
public class LogServiceImpl implements LogService {
    @Autowired
    StarterProperties properties;

    @Override
    public String log(String logLine) {
        System.out.println("Log service call:" + logLine + "---" + properties.getLabelText());
        return logLine;
    }
}
