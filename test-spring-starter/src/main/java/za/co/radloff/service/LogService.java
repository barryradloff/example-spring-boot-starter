package za.co.radloff.service;

/**
 * Created by barry on 2017/06/14.
 */
public interface LogService {
    public String log(String logLine);
}
