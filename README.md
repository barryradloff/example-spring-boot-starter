# Spring Boot Starter Example #

### Table of Contents
 1. [What it does](#What it does)
 1. [How to build](#How to build)
 1. [How to use](#How to use)


## What it does
This project is to demonstrate how to make a starter project.

It consists of two projects. 

 - test-spring-starter <--This is the starter module
 
 - using-spring-start-module <-- This is the module that uses the starter module
 
## How to build 
In each directory run ```mvn install```

## How to use
In the using-spring-start-module directory run ```mvn spring-boot:run```
