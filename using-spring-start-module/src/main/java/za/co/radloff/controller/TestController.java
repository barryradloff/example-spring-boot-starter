package za.co.radloff.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.radloff.service.LogService;

/**
 * Created by barry on 2017/06/14.
 */
@RestController
@RequestMapping("/api/test/v1")
public class TestController {


    private final LogService log;
    @Autowired
    public TestController(final LogService logService){
        this.log = logService;
    }

    @GetMapping("/")
    public String test(){
        return log.log("this");
    }
}
