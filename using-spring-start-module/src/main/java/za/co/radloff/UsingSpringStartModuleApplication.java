package za.co.radloff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UsingSpringStartModuleApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsingSpringStartModuleApplication.class, args);
	}
}
